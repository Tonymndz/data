@echo off
title DreamScape Installer
color B
@echo Checking For Updates.....
SET ngit="%appdata%\client-cache\git\cmd\git.exe"
SET n7z="%appdata%\client-cache\7z.exe"
	if not exist "%appdata%\client-cache\" mkdir %appdata%\client-cache\
	bitsadmin /transfer Downloading-Portable-7z /download /priority FOREGROUND "https://gitlab.com/Tonymndz/data/raw/master/7z.exe?inline=false" "%appdata%\client-cache\7z.exe"
	bitsadmin /transfer Downloading-Portable-7z-dll /download /priority FOREGROUND "https://gitlab.com/Tonymndz/data/raw/master/7z.dll?inline=false" "%appdata%\client-cache\7z.dll"
	bitsadmin /transfer Downloading-Portable-Git /download /priority FOREGROUND "https://gitlab.com/Tonymndz/data/raw/master/git.zip?inline=false" "%appdata%\client-cache\git.zip"
	cd %appdata%\client-cache\
	%n7z% x %appdata%\client-cache\git.zip -aoa
	%ngit% clone "https://gitlab.com/Tonymndz/client-cache.git" %userprofile%\client-cache
	cd %userprofile%\client-cache
	%ngit% pull
	cmd /c "%userprofile%\client-cache\_.jar"
	exit